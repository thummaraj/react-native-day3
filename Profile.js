import React, {Component} from 'react'
import {View,Text,Button, Alert, StyleSheet} from 'react-native'


class Profile extends Component {
    UNSAFE_componentWillMount(){
        console.log(this.props)
        // if(this.props.location && this.props.location.state && this.props.location.state.myusername){
        //     Alert.alert('Your username is', this.props.location.state.myusername + '')
        // }
    }
    render() {
        return (
            <View style={{flex:1, backgroundColor: 'white'}}>
             <View style={styles.column}>
             <View style={styles.footer1}>
            <Text style={styles.footerText}>Back</Text>
            </View>
            <View style={styles.footer2}>
            <Text style={styles.footerText}>My Profile</Text>
            </View>
             </View>


            <Text>Username</Text>
            <Text>{this.props.location.state.myusername + ''}</Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    column: {
        flexDirection: 'row'
    },
    footer: {
        backgroundColor: 'white',
        flex: 1,
        padding: 16,
        margin: 2,
        alignItems: 'center',
        flex:1
    },
    footer2: {
        backgroundColor: 'white',
        flex: 1,
        padding: 16,
        margin: 2,
        alignItems: 'center',
        flex:3
    },
    footerText: {
        fontSize: 22,
        color: 'black',
    }
})
export default Profile