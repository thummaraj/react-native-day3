import React, {Component} from 'react'
import {View,Text} from 'react-native'
import { Route, NativeRouter, Switch, Redirect} from 'react-router-native'
import Sceen1 from './Sceen1'
import Sceen2 from './Sceen2'
import Profile from './Profile';
import Login from './Login';

class Router extends Component {
    render() {
        return (
           <NativeRouter>
               <Switch>
                   {/* <Route exact path="/sceen1" component={Sceen1} />
                   <Route exact path="/sceen2" component={Sceen2} /> */}
                   <Route exact path="/login" component={Login} />
                   <Route exact path="/profile" component={Profile} />
                   <Redirect to="/login"/>
               </Switch>
           </NativeRouter>
        )
    }
}
export default Router