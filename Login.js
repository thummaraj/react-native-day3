import React, {Component} from 'react'
import {View,Text, Alert, Button, TextInput, StyleSheet, Image} from 'react-native'
import {Link} from 'react-router-native'

class Login extends Component {
    state = {
        username: '',
        password: ''
    }

    goToLogin=()=> {
        this.props.history.push('/profile', {
            myusername: this.state.username
        })
    }
    render() {
        return (
            <View style={{flex:1, backgroundColor: 'white'}}>
           <View style={[styles.layer1, styles.center]}>
            <View style={[styles.circle, styles.center]}>
            <Image source={require('./cat.jpg')} style={[styles.circle, styles.center]} />
            </View>
            </View>
            
            <View  style={styles.layer2}>
                <Text style={{ alignItems: 'center'}}>username: {this.state.username} - password: {this.state.password}</Text>
            <TextInput onChangeText={ (username) => {this.setState({username})}}  
            style={{ height: 40, borderColor: 'gray', borderWidth: 1, backgroundColor: 'white', margin: 14 }}></TextInput>
            <TextInput onChangeText={ (value) => {this.setState({password: value})}}  
            style={{ height: 40, borderColor: 'gray', borderWidth: 1, backgroundColor: 'white', margin: 14 }}></TextInput>
            <Button style={styles.rectangleText2} title="Login" onPress={this.goToLogin}/>
            </View>

            </View>
        )
    }
}
const styles = StyleSheet.create({
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    circle: {
        width: 200,
        height: 200,
        borderRadius: 200/2,
        backgroundColor: 'gray',
    },
    layer1: {
        
        flex: 1,
        
        
    },
    layer2: {
        
        flex: 1,
        
    },
    rectangle: {
        backgroundColor: 'white',
        padding: 20,
        margin: 14
    
    },
    rectangle1: {
        backgroundColor: 'black',
        padding: 20,
        margin: 14
    
    },
    rectangleText: {
        color: 'black',
    },
    rectangleText2: {
        color: 'white',
        
        

    },

})
export default Login