import React, {Component} from 'react'
import {View,Text,Button} from 'react-native'


class Sceen2 extends Component {
    goToSceen1 = () => {
        this.props.history.push('/sceen1', {
            mynumber:20
        })
    }
    render() {
        return (
            <View style={{flex:1, backgroundColor: 'pink'}}>
            <Text style={{color: 'white'}}>Sceen2</Text>
            <Button title="Go Go" onPress={this.goToSceen1}/>
            </View>
        )
    }
}

export default Sceen2