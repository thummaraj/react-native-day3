import React, {Component} from 'react'
import {View,Text, Alert} from 'react-native'
import {Link} from 'react-router-native'

class Sceen1 extends Component {
    UNSAFE_componentWillMount(){
        console.log(this.props)
        if(this.props.location && this.props.location.state && this.props.location.state.mynumber){
            Alert.alert('Your number is', this.props.location.state.mynumber + '')
        }
    }
    render() {
        return (
            <View style={{flex:1, backgroundColor: 'red'}}>
            <Text style={{color: 'white'}}>Sceen1</Text>
            <Link to="/sceen2">
            <Text style={{color: 'white'}}>Go to sceen2</Text>
            </Link>
            </View>
        )
    }
}
export default Sceen1